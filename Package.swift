// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let version = "17.10.1"
let checksum = "7b4badcf0afc99e853e459590ae9662d3ed3577d46d205510b735c6173fe32f9"

let package = Package(
    name: "vibe-ios-spm-vibesdk",
    products: [
        .library(
            name: "VibeSDK",
            targets: [
                "VibeSDK"
            ]
        ),
    ],
    targets: [
        .binaryTarget(
                    name: "VibeSDK",
                    url: "https://axonvibe.jfrog.io/artifactory/ios-sdk-spm/VibeSDK/\(version)/VibeSDK.zip",
                    checksum: checksum
                )
        ]
)
