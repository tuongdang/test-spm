# Process Release a new version (VibeSDK)
- VibeSDK: 17.10.1
## 1: Get repository
- git clone https://gitlab.com/axonvibe/chapter-vn-ios/vibe-ios-spm-vibesdk
- checkout to branch master
## 2: Update VibeSDK to required version
The VibeSDK using by the app. All versions are listed below reflect what we have on the `axonvibe.jfrog.io` folder after running `./scripts/vibe-bootstrap.sh` command.

1. To get specific BETA version. Could be combined with -f to force sync.
./scripts/vibe-bootstrap.sh -b 16.1.0-2678

2. Auto detecting the BETA version from the README.md file. This method would be useful for your CI/CD tool.
./scripts/vibe-bootstrap.sh -b

3. To get specific RELEASE version. Could be combined with -f to force sync.
./scripts/vibe-bootstrap.sh -r 16.1.0

4. Auto detecting the RELEASE version from the README.md file. This method would be useful for your CI/CD tool.
./scripts/vibe-bootstrap.sh -r
## 3: Commit all change to master
- git add .
- git commit -m "Update VibeSDK version ...."
- git push origin master
## 4: Release new version
- git checkout release
- git merge master
- git tag `<VibeSDK version>`  -> eg: `17.3.0`
- git commit -m "Release VibeSDK `<VibeSDK version>`"
- git push origin master --tags
- Or you can run script `./script/vibe-distribute.sh <VibeSDK version>`
## 5: Update new version of VibeSDK to Repository is using this.
- get hash commit tag of version release
- git rev-list -n 1 `<VibeSDK version>` -> eg: `17.3.0`
- copy hash commit above
- Update (hash commit) to Package Dependencies of the repository is using this.

Note: Don't for get to commit all changes on the README.md file to get the expected results!
