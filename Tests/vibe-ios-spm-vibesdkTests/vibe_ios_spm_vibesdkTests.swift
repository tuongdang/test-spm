import XCTest
@testable import vibe_ios_spm_vibesdk

final class vibe_ios_spm_vibesdkTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(vibe_ios_spm_vibesdk().text, "Hello, World!")
    }
}
