#!/bin/bash -e

# 🥕Constants
VERSION=$1
CHECKSUM=$2

echo "[✔︎] Begin package.swift"

function process_infomation {
    echo "👉 Update version VibeSDK to README"
    sed -i '' -e "s/VibeSDK: .*/VibeSDK: $VERSION/" $WORKSPACE/README.md

    echo "👉 Update version information in package manifest"
    # replace version information in package manifest
    sed -E -i '' 's/let version = ".+"/let version = "'$VERSION\"/ $WORKSPACE/Package.swift
    # replace kind of VibeSDK is release or snapshot in package manifest
    sed -E -i '' 's/let path = ".+"/let path = "'$CHECKSUM\"/ $WORKSPACE/Package.swift
}

process_infomation

echo "[✔︎] Updated package.swift"
