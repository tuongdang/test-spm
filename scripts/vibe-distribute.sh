#!/bin/bash

VERSION=$1

function distribute_git_step_1 {
    echo "[✅] Add changed"
    git add .
    echo "[✅] Commit"
    git commit -m "Update VibeSDK version $VERSION"
    echo "[✅] Master"
    git push origin master
}

function verify_version {
    if [[ -z $VERSION ]]; then
        echo "[❌] Missing version number! Please input verions number. Eg: 17.4.0"
        exit 1
    fi
}

function distribute_git_step_2 {
    echo "[📲] Merge Master branch to Release branch"
    git checkout release
    git merge master
    echo "[✅] Distribute git complete!"
    
    echo "[📌] Add tag with version $VERSION"
    git tag $VERSION
    echo "[✅] Add tag complete!"
    
    echo "[✉️] Distribute git"
    git commit -m "Release VibeSDK version $VERSION"
    git push origin master --tags
    echo "[✅] Distribute git complete!"
}

verify_version
distribute_git_step_1
distribute_git_step_2
